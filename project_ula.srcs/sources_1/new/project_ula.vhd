library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity project_ula is
    Port ( clk : in STD_LOGIC;
           a : in STD_LOGIC_VECTOR(3 DOWNTO 0);
           b : in STD_LOGIC_VECTOR(3 DOWNTO 0);
           modo : in STD_LOGIC;
           clear : in STD_LOGIC;
           enter : in STD_LOGIC;
           run : in STD_LOGIC;
           oper : in STD_LOGIC_VECTOR(3 DOWNTO 0);
           bin: in STD_LOGIC;
           enable: in STD_LOGIC;
           an : out STD_LOGIC_VECTOR (3 downto 0);
           led: out STD_LOGIC_VECTOR(3 DOWNTO 0);
           seg : out STD_LOGIC_VECTOR(7 downto 0));
end project_ula;

architecture Behavioral of project_ula is

	component comp_operacoes is
        Port ( oper : in STD_LOGIC_VECTOR(3 DOWNTO 0);
	    	   a : in STD_LOGIC_VECTOR(3 DOWNTO 0);
	    	   b : in STD_LOGIC_VECTOR(3 DOWNTO 0);
	    	    s_res : out STD_LOGIC_VECTOR(3 DOWNTO 0);
	    	   erro: out STD_LOGIC);
   end component;
   component comp_programa is
        Port ( clk: in STD_lOGIC;
              enter : in STD_LOGIC; 
              run: in STD_LOGIC; 
              clear : in STD_LOGIC;
              a: in STD_LOGIC_VECTOR(3 DOWNTO 0); 
              oper : in STD_LOGIC_VECTOR(3 downto 0); 
              oper_out : out STD_LOGIC_VECTOR(3 downto 0); 
              end_out : out STD_LOGIC_VECTOR(3 downto 0); 
              res_out : out STD_LOGIC_VECTOR(3 downto 0)); 
   end component;
   
   component Decod_BCD is
       Port ( s_res : in STD_LOGIC_VECTOR (3 downto 0);
              s_seg_1, s_seg_2, s_seg_3, s_seg_4 : out STD_LOGIC_VECTOR (7 downto 0));
   end component;
   
   component comp_decod_dec is
       Port ( s_res : in STD_LOGIC_VECTOR (3 downto 0);
              s_seg_1 : out STD_LOGIC_VECTOR (7 downto 0);
              s_seg_2 : out STD_LOGIC_VECTOR (7 downto 0);
              s_seg_3 : out STD_LOGIC_VECTOR (7 downto 0);
              s_seg_4 : out STD_LOGIC_VECTOR (7 downto 0));
   end component;
-- resultados
  signal s_res_oper: STD_LOGIC_VECTOR(3 DOWNTO 0);
  signal s_res: STD_LOGIC_VECTOR(3 DOWNTO 0);
  signal s_erro: STD_LOGIC;
-- sinais para funï¿½ï¿½o programa
  signal s_res_out: STD_LOGIC_VECTOR(3 DOWNTO 0);  
-- sinais para multiplexacao dos displays
  signal s_clk_count: std_logic_vector(17 downto 0) := (others => '0');
  signal s_clk_390_Hz: std_logic := '0';
  constant C_195HZ: std_logic_vector (17 downto 0) := "111110100110011010";
  signal s_count: std_logic_vector (1 downto 0) := (others => '0');
  signal s_an: std_logic_vector (3 downto 0) := (others => '0');
  signal s_seg_1_bin: std_logic_vector (7 downto 0) := (others => '0');
  signal s_seg_2_bin: std_logic_vector (7 downto 0) := (others => '0');
  signal s_seg_3_bin: std_logic_vector (7 downto 0) := (others => '0');
  signal s_seg_4_bin: std_logic_vector (7 downto 0) := (others => '0');
  signal s_seg_1_dec: std_logic_vector (7 downto 0) := (others => '0');
  signal s_seg_2_dec: std_logic_vector (7 downto 0) := (others => '0');
  signal s_seg_3_dec: std_logic_vector (7 downto 0) := (others => '0');
  signal s_seg_4_dec: std_logic_vector (7 downto 0) := (others => '0');
  signal s_seg_1: std_logic_vector (7 downto 0) := (others => '0');
  signal s_seg_2: std_logic_vector (7 downto 0) := (others => '0');
  signal s_seg_3: std_logic_vector (7 downto 0) := (others => '0');
  signal s_seg_4: std_logic_vector (7 downto 0) := (others => '0');
  
  signal oper_out :STD_LOGIC_VECTOR(3 downto 0); 
  signal end_out : STD_LOGIC_VECTOR(3 downto 0); 
  
begin
	CALCULA: comp_operacoes port map
	 (	
		oper => oper,
		a => a,
		b => b,
		s_res => s_res_oper,
		erro => s_erro
	 );
  
    PROGRAMA: comp_programa port map
    (
       clk => clk,
       enter => enter,
       run => run,
       clear => clear,
       a => a,
       oper => oper,
       oper_out => oper_out,
       end_out => end_out,
       res_out => s_res_out
    );
     
    an <= s_an;
    
    
    RESULTADO: process (modo, oper,a,b)
        begin
            case modo is
                when '0' => -- modo calculadora
                    s_res <= s_res_oper;
                when '1' => --  modo programa
                    if  (enable = '0') then
                    	s_res <= oper_out;
                    	led <= end_out;
                    elsif (enable = '1') then
                    	s_res <= s_res_out;
                    end if;
            end case;
    end process;

-- processos que multiplexam os displays   
    CLK_DIV_PROC: process (clk, s_count)
        begin
            if rising_edge(clk) then                 
                if s_clk_count = C_195HZ then
                    s_clk_count <= (others => '0');
                    s_clk_390_Hz     <= not s_clk_390_Hz;           
                else
                    s_clk_count <= std_logic_vector(unsigned(s_clk_count) + to_unsigned(1,18));
                end if;
            end if;
    end process;
           
    CLK_PROC: process(s_clk_390_Hz)
        begin
            if rising_edge(s_clk_390_Hz) then
                s_count <= std_logic_vector (unsigned(s_count) + to_unsigned(1,2));
            end if;
        end process;
            
    ANODO_PROC: process(s_clk_390_Hz, s_count)
        begin
            if rising_edge(s_clk_390_Hz) then
                if s_count = "00" then 
                    s_an <= "1110";
                    seg  <= s_seg_1 ; -- primeiro display (esquerda pra direita)
                elsif s_count = "01" then
                    s_an <= "1101";
                    seg  <= s_seg_2; -- segundo display
                elsif s_count = "10" then
                    s_an <= "1011";
                    seg  <= s_seg_3; -- terceiro display
                elsif s_count = "11" then
                    s_an <= "0111";
                    seg  <= s_seg_4; --  quarto display
                else
                    s_an <= "1111";
                    seg  <= "11111111";
                end if;
             end if;
    end process;

    Disp_Bin: Decod_BCD port map --saidas em binario
              (
               s_res => s_res,
               s_seg_1 => s_seg_1_bin,
               s_seg_2 => s_seg_2_bin,
               s_seg_3 => s_seg_3_bin,
               s_seg_4 => s_seg_4_bin                      
              );
              
    Disp_dec: comp_decod_dec port map
              (
              s_res => s_res,
              s_seg_1 => s_seg_1_dec,
              s_seg_2 => s_seg_2_dec,
              s_seg_3 => s_seg_3_dec,
              s_seg_4 => s_seg_4_dec   
              );
             
    DECOD_SAIDA_PROC: process(s_res, s_erro, bin, modo)
        begin
            case modo is
                when '0' =>
                	if (bin = '1') and (s_erro = '0') then
                		s_seg_1 <= s_seg_1_bin;
                    	s_seg_2 <= s_seg_2_bin;
	                    s_seg_3 <= s_seg_3_bin;
    	                s_seg_4 <= s_seg_4_bin;
        	        elsif (bin = '0') and (s_erro = '0') then
            	        s_seg_1 <= s_seg_1_dec;
                	    s_seg_2 <= s_seg_2_dec;
                    	s_seg_3 <= s_seg_3_dec;
       		            s_seg_4 <= s_seg_4_dec; 
       		        elsif (s_erro = '1') then
       		        	s_seg_1 <= "11000000"; --O
       		            s_seg_2 <= "10001000"; --R
       		            s_seg_3 <= "10001000"; --R
       		            s_seg_4 <= "10000110"; --E
       		        else
       	                s_seg_1 <= "11111111"; --apagado
       		        	s_seg_2 <= "11111111"; --apagado
	       		        s_seg_3 <= "11111111"; --apagado
    	   		        s_seg_4 <= "11111111"; --apagado            
                	end if;                   
                when '1' =>
                	s_seg_1 <= s_seg_1_bin;
                    s_seg_2 <= s_seg_2_bin;
	            	s_seg_3 <= s_seg_3_bin;
               	    s_seg_4 <= s_seg_4_bin;
            end case;
          
    end process;
end Behavioral;
