library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity Decod_BCD is
    Port ( s_res : in STD_LOGIC_VECTOR (3 downto 0);
           s_seg_1, s_seg_2, s_seg_3, s_seg_4 : out STD_LOGIC_VECTOR (7 downto 0));
end Decod_BCD;

architecture Behavioral of Decod_BCD is

begin
    
        with s_res(0) select
            s_seg_1 <= "11000000" when '0',
                       "11111001" when '1';
                       
        with s_res(1) select
            s_seg_2 <= "11000000" when '0',
                       "11111001" when '1';
                       
        with s_res(2) select
            s_seg_3 <= "11000000" when '0',
                       "11111001" when '1';
                       
        with s_res(3) select
            s_seg_4 <= "11000000" when '0',
                       "11111001" when '1';

end Behavioral;
