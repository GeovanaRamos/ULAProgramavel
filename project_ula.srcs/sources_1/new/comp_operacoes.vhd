library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;
use IEEE.STD_LOGIC_UNSIGNED.ALL;


entity comp_operacoes is
    Port ( oper : in STD_LOGIC_VECTOR(3 DOWNTO 0);
    	   a : in STD_LOGIC_VECTOR(3 DOWNTO 0);
    	   b : in STD_LOGIC_VECTOR(3 DOWNTO 0);
    	   s_res : out STD_LOGIC_VECTOR(3 DOWNTO 0);
    	   erro: out STD_LOGIC);
    	   
end comp_operacoes;

architecture Behavioral of comp_operacoes is

  signal s_soma: STD_LOGIC_VECTOR(4 DOWNTO 0);
  signal s_sub: STD_LOGIC_VECTOR(4 DOWNTO 0);
  signal s_mult: STD_LOGIC_VECTOR(9 DOWNTO 0);
  signal s_div: STD_LOGIC_VECTOR(3 DOWNTO 0);
  signal s_mod: STD_LOGIC_VECTOR(3 DOWNTO 0);    
  signal s_sqa: STD_LOGIC_VECTOR(7 DOWNTO 0);
  signal s_neg: STD_LOGIC_VECTOR(3 DOWNTO 0);
  signal s_add: STD_LOGIC_VECTOR(3 DOWNTO 0);

  
begin

	process (oper,a,b) --Variável "modo" não está sendo usada
        begin
        erro <= '0';
    	case oper is
         	when "0000" => -- zerar o resultado
            	s_res <= "0000";
            when "0001" => -- resultado todo 1111
                s_res <= "1111";
            when "0010" => -- mostra a
                s_res <= a;
            when "0011" => -- mostra b
                s_res <= b;
            when "0100" => -- or 
                s_res <= a or b;
            when "0101" => -- and
                s_res <= a and b;
            when "0110" => -- xor
                s_res <= a xor b;
            when "0111" => -- not
                s_res <= not(a);
            when "1000" => -- soma
                s_soma <=  std_logic_vector(to_unsigned(to_integer(unsigned(a)+ unsigned(b)), s_soma'length));
                if ((a(3) = b(3)) and (a(3) /= s_soma(3))) or s_soma(4)= '1'   then -- verificando overflow
                	erro <= '1'; 
                else -- se n�o tem erro, pegar o resultado
                	s_res <= s_soma(3 DOWNTO 0);
                end if;
            when "1001" => -- subtra��o
            	s_sub <= std_logic_vector(to_unsigned(to_integer(unsigned(b) - unsigned(a)), s_sub'length));
            	if s_sub(4)= '1' then -- verificando se tem carry 
                	erro <= '1';
                elsif (a(3) /= b(3)) and (a(3) /= s_sub(3)) then -- verificando overflow 
                    erro <= '1';
                else -- se n�o tem erro, pegar o resultado
                    s_res <= s_sub(3 DOWNTO 0);
                end if;
			when "1010" => --multiplica��o
          		s_mult <= ('0' & a)*('0' & b); 
                if a*b > "1111" then --vrificando se tem overflow
                        erro <= '1'; 
                else --se n�o tem erro, pegar o resultado
                        s_res <= s_mult(3 downto 0);
                end if;
                       
 			when "1011" => -- divis�o
                s_div <= std_logic_vector(to_unsigned(to_integer(unsigned(a)/ unsigned(b)), s_div'length));
                if std_logic_vector(to_unsigned(to_integer(unsigned(b)/ unsigned(a)), s_div'length)) > "1111" then --verificando se tem overflow
                    erro <= '1'; 
                else --se n�o tem erro, pegar o resultado
                    s_res <= s_div(3 downto 0);
                end if;
                                                   
            when "1100" => -- m�dulo
                s_mod <= std_logic_vector(to_unsigned(to_integer(unsigned(a) mod unsigned(b)), s_div'length));
                s_res <= s_mod;
            when "1101" => -- elevar ao quadrado
          		s_sqa <=  a*a;
                if s_sqa > "1111" then --verificando se tem overflow
                    erro <= '1'; 
                elsif s_sqa(4)= '1' then -- verificando se tem carry
                    erro <= '1';
                else --se n�o tem erro, pegar o resultado
                    s_res <= s_sqa(3 DOWNTO 0);
                end if;
            when "1110" => --negativo
                s_neg <= not(a) + '1';
                s_res <= s_neg;
            when "1111" => --incrementar
                s_add <= a + '1';
                s_res <= s_add(3 DOWNTO 0);
    	end case;
    end process;
end Behavioral;
