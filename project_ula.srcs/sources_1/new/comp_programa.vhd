library IEEE;
use IEEE.STD_LOGIC_1164.ALL;	 
use IEEE.STD_LOGIC_UNSIGNED.ALL; 
use IEEE.NUMERIC_STD.ALL;

entity comp_programa is
    Port (  clk: in STD_LOGIC;
            enter : in STD_LOGIC; 
            run: in STD_LOGIC; 
            clear : in STD_LOGIC;
            a: in STD_LOGIC_VECTOR(3 DOWNTO 0); 
            oper : in STD_LOGIC_VECTOR(3 downto 0); 
            oper_out : out STD_LOGIC_VECTOR(3 downto 0);
            end_out : out STD_LOGIC_VECTOR(3 downto 0); 
            res_out : out STD_LOGIC_VECTOR(3 downto 0)); 
end comp_programa;

architecture Behavioral of comp_programa is

   component comp_operacoes is
        Port ( oper : in STD_LOGIC_VECTOR(3 DOWNTO 0);
	    	   a : in STD_LOGIC_VECTOR(3 DOWNTO 0);
	    	   b : in STD_LOGIC_VECTOR(3 DOWNTO 0);
	    	   s_res : out STD_LOGIC_VECTOR(3 DOWNTO 0);
	    	   erro: out STD_LOGIC);
   end component;

   
  signal s_oper: STD_LOGIC_VECTOR(3 DOWNTO 0);
  signal fim: std_logic := '0';
  signal count : integer range 0 to 4 := 0;
  signal s_count : integer range 0 to 4 := 0;
  
  type vector_array is array(0 to 4) of std_logic_vector(3 downto 0);	
  signal memory : vector_array;	
 
  signal s_erro: STD_LOGIC;
  signal s_res: STD_LOGIC_VECTOR(3 DOWNTO 0);
  signal s_erro2: STD_LOGIC;
  signal s_res2: STD_LOGIC_VECTOR(3 DOWNTO 0);
  signal tmp: std_logic_vector(3 downto 0):= "0000"; 


begin
	CALCULA_AB: comp_operacoes port map
	 ( 	oper => s_oper,
		a => a,
		b => tmp,
		s_res => s_res,
		erro => s_erro
	 );
	 
	 CALCULA_A: comp_operacoes port map
	  ( oper => s_oper,
	 	a => tmp,
	 	b => tmp,
	 	s_res => s_res2,
	 	erro => s_erro2
	  );
    
	process(enter)
	 	begin	
	 	if rising_edge(enter) then	
	 		if (count = 4) then 
	 			count <= 1;
	 		else
	 			count <= count + 1;	
	 		end if;
	 		oper_out <= oper;
	 		end_out <= std_logic_vector(to_unsigned(count, 4));
	 		memory(count) <= oper;
	 	end if;
	 end process;
 
	 process (run) 
    	begin 
    		if rising_edge(run) then
    			if (clear = '0') then
					if (s_count = 4) then 
						s_count <= 1;
					else
						s_count <= count + 1;	
					end if;
					s_oper <= memory(s_count);
					-- para operacoes entre A e B
					if (s_oper = "0100" or s_oper = "0101" or s_oper = "0110" or s_oper = "0111" or s_oper = "1000" or s_oper="1001" or s_oper="1010" or s_oper="1011" or s_oper="1100") then 
						tmp <= s_res; 
					-- para operacoes s� com A
					else
						if (s_count = 1) then
							tmp <= s_res;
						else
							tmp <= s_res2;
						end if;
						
					end if;
				else 
					s_count <= 0;
					tmp <= "0000";
				end if; 
	 		end if;
	 		
   	end process; 
   	res_out <= tmp; 
   	
end Behavioral;
