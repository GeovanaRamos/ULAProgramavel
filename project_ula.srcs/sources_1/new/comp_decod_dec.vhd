library IEEE;
use IEEE.STD_LOGIC_1164.ALL;


entity comp_decod_dec is
    Port ( s_res : in STD_LOGIC_VECTOR (3 downto 0);
           s_seg_1 : out STD_LOGIC_VECTOR (7 downto 0);
           s_seg_2 : out STD_LOGIC_VECTOR (7 downto 0);
           s_seg_3 : out STD_LOGIC_VECTOR (7 downto 0);
           s_seg_4 : out STD_LOGIC_VECTOR (7 downto 0));
end comp_decod_dec;

architecture Behavioral of comp_decod_dec is

begin
    process (s_res)
    begin
        case s_res is
            when "0000" => 
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11000000"; --0
                s_seg_1 <= "11000000"; --0
            when "0001" => 
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11000000"; --0
                s_seg_1 <= "11111001"; --1
            when "0010" =>
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11000000"; --0
                s_seg_1 <= "10100100"; --2
            when "0011" => 
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11000000"; --0
                s_seg_1 <= "10110000"; --3
            when "0100" =>
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11000000"; --0
                s_seg_1 <= "10011001"; --4
            when "0101" =>
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11000000"; --0
                s_seg_1 <= "10010010"; --5
            when "0110" => 
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11000000"; --0
                s_seg_1 <= "10000010"; --6
            when "0111" => 
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11000000"; --0
                s_seg_1 <= "11111000"; --7
            when "1000" => 
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11000000"; --0
                s_seg_1 <= "10000000"; --8
            when "1001" => 
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11000000"; --0
                s_seg_1 <= "10010000"; --9
            when "1010" => 
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11111001"; --1
                s_seg_1 <= "11000000"; --0                           
            when "1011" =>
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11111001"; --1
                s_seg_1 <= "11111001"; --1
            when "1100" =>
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11111001"; --1
                s_seg_1 <= "10100100"; --2
            when "1101" => 
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11111001"; --1
                s_seg_1 <= "10110000"; --3
            when "1110" => 
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11111001"; --1
                s_seg_1 <= "10011001"; --4
            when "1111" => 
                s_seg_4 <= "11000000"; --0
                s_seg_3 <= "11000000"; --0
                s_seg_2 <= "11111001"; --1
                s_seg_1 <= "10010010"; --5              
        end case;  
    end process;
end Behavioral;
